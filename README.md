# Verwaltung der Sicherheitsfirmen

## This project is currently under development. 80% of the back-end section and 30% of the front-end section. Unfortunately, due to project data protection, I cannot post more information in the public space.


## A system of automatic assignment of workers to the demand for workers Use of artificial intelligence. Connection of security companies and workers to a central network.
 

#### This software focuses on one goal with two different applications, so the core of the software organizes the internal part of a security company. Then, by creating a circle of communication between its users, "request manpower, company providing manpower, company staff," And finally, through the connection to partner companies, a cooperation network is created between activists in this area.After retrieving the information from the database, the software uses artificial intelligence to identify free and suitable employees a nd send them the proposal for this assignment. The system waits a certain amount of time for an answer. When the system receives a response from company personnel, it assigns the most appropriate agent to the request. Otherwise, the system immediately sends its proposal to partner companies; this process continues until the force is found and assigned. After assigning the personnel, first register the information in the database and notify the personnel, the requester, and the managers of the company of the successful action "Assignment of the task force and the required information."
### This software has four users or four different access levels, which you can see below.
- First level: Admin
- Second level: Planer
- third level: Employee or subcompany
- forth level: Assigner 


### Problem description
- Company costs for planning and organizing the work
- Abolition of the traditional system of request and attribution of force
- A lot of time to allocate the right workforce, and the possibility of human error
- Lack of decision-making and employee satisfaction with the operations manager's work schedule
- Lack of an efficient and centralized communication system
- It is impossible for managers to control employees in real time and online
- The inability to obtain and document customer feedback on the quality of staffing service
- Manual and human searches to find free labor or according to the request you want
- Unequal distribution of work among employees
- Hard access to staff when replacements are needed
- High stress in finding patient replacements
- Traditional contacts with SUP companies
- Lack of time tracking
- No gear storage system
- Missing work schedules due to a lot of information in rosters
- Headaches and worker dissatisfaction during public holidays
  ## company goals
  - Submission and online monitoring of objects and missions by administrators
  - Automatic and semi-automatic rosters
  - Centralized communication system between staff
  - High speed in assigning substitutes
  - Control of personnel and service quality
  - Online staff check-in and check-out based on location
  - Chatting system
  - Time tracking
  - Equipment storage system
  - Event announcement, registration, and tracking system
  - Find and allocate passenger replacements for emergencies
  - If you cannot find a replacement, send the application to the subcontractor
  - Reducing the costs of companies by eliminating excess forces for planning and organizing work
  - Greater staff satisfaction from the work schedule
  - Accurate and online submission system
  

# Based on these requirements, I have created the following database model in SQL Server:
<img src="DatabaseDiagram.jpg" title=" Diagram" width=1200 >

## You can see the list of entities below
- Agency
- Assigner
- AssignerResponsible
- AuthorityGroup
- AuthorityToken
- AuthorityTokenAuthorityToken
- Avatar
- AvatarClothes
- Capability
- Clothes
- Contract
- Employee
- EmployAuthorityGroup
- EmployeeCapability
- EmployMission
- EmployeeMission
- EmployeeMissionEvent
- EmployeeMissionEventFile
- EmployeeMissionRate
- EmployeeVacation
- Equipment
- Instruction
- InstructionFile
- Object
- ObjectEquipement
- ObjectInstruction
- ObjectProgrammer
- ObjectRequiredCapability
- ObjectSecurityEmployee
- ObjectShift
- ObjectShiftType
- ObjectWorkDay
- ObjectWorkDayShift
- Question
- QuestionAnswer
- QuestionAnswerDetail
- QuestionOption
- Request
- Ticket
- TicketCategory
- User
- UserProfile
- UserToken
- WorkSheet
  

